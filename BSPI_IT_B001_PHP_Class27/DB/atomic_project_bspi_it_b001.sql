-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2019 at 11:33 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_bspi_it_b001`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_bspi_it_b001` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_bspi_it_b001`;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(13) NOT NULL,
  `first_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `roll` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `home_district` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bangla_mark` float NOT NULL,
  `english_mark` float NOT NULL,
  `math_mark` float NOT NULL,
  `remarks` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `roll`, `dob`, `gender`, `hobbies`, `profile_picture`, `home_district`, `bangla_mark`, `english_mark`, `math_mark`, `remarks`, `is_trashed`) VALUES
(2, 'Code Station', 'Technical Training Center', '3749298748', '2019-05-14', 'Male', 'Reading, Music, Programming', 'bin1559190563.jpg', 'Noakhali', 56, 88, 44, 'nothing', 'NO'),
(3, 'Code Station', 'Technical Training Center', '3749298748', '2019-05-14', 'Male', 'Reading, Music, Programming', 'bin1559191142.jpg', 'Noakhali', 56, 88, 44, 'nothing', 'NO'),
(4, 'ABC1', 'XYZ', '3749298748', '2019-06-24', 'Male', 'Reading, Programming', 'azhmasum1561804516.jpg', 'Noakhali', 45, 75, 44, 'sdfsdf', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
