<?php

   require_once "../vendor/autoload.php";
   use App\Utility;

   $objStudent = new \App\Student();


   if($_FILES['ProfilePicture']['name']!="")
   {
       $fnArr =  explode(".", $_FILES['ProfilePicture']['name']);
       $fileName =   $fnArr[0].time().".".$fnArr[1];
       $source = $_FILES['ProfilePicture']['tmp_name'];
       $destination = "../resources/ProfilePictures/" . $fileName;
       move_uploaded_file($source , $destination);

   }
   else{
       $objStudent->setData($_POST);
       $singleRecord = $objStudent->view();
       $fileName = $singleRecord->profile_picture;
   }


   $_POST['ProfilePicture'] = $fileName;


   $_POST['Hobbies'] = implode(", ", $_POST['Hobbies']);

   $objStudent->setData($_POST);

   $objStudent->update();

   Utility::redirect("index.php");


