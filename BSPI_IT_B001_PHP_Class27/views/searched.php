<?php
ob_start();
require_once "header.php";
require_once "../vendor/autoload.php";
$objStudent = new \App\Student();



use App\Message;



################## search  block 4 of 5 start ##################

$refURL = $_SERVER['HTTP_REFERER'];

$allRecords = $objStudent->search($_REQUEST);

if(count($allRecords)>0) {
    Message::message(count($allRecords) . " Records Found");
}
else{
    Message::message(" No Search Result Found");

}

################## search  block 4 of 5 end ##################


?>




<h4 class="Middle">Student Information -  All (Active/Trashed) - Search Results    </h4>



<form  class="Middle" method="post" id="MultipleCheckBoxForm" style="padding-top: 30px">


    <table id="DataTable" class="display table table-bordered table-striped" style="width:100%">
        <thead>
        <tr>

            <th class='Center'> Select All <input id = "SelectAll" type="checkbox" > </th>
            <th>  Serial </th>
            <th>  ID </th>
            <th>  First Name </th>
            <th>  Last Name </th>
            <th>Picture</th>
            <th>Home District</th>
            <th>Hobbies</th>
            <th>  Roll </th>
            <th>  Date of Birth </th>
            <th>  Gender </th>
            <th>Bangla</th>
            <th>English</th>
            <th>Math</th>
            <th>Remarks</th>
            <th>Action Buttons</th>


        </tr>
        </thead>
        <tbody>


        <?php
        $serial = 1;

        foreach ($allRecords as $singleRecord){

            echo "
             
                   <tr>  
                       
                       <td class='Center'> <input class='checkboxs' type='checkbox' name='selected[]' value=$singleRecord->id> </td>  

                       <td>$serial</td>
                       <td>$singleRecord->id</td>
                       <td>$singleRecord->first_name</td>
                       <td>$singleRecord->last_name</td>
                       <td><img width='75' height='75' src='../resources/ProfilePictures/$singleRecord->profile_picture'></td>
                       <td>$singleRecord->home_district</td>
                       <td>$singleRecord->hobbies</td>
                       <td>$singleRecord->roll</td>
                       <td>$singleRecord->dob</td>
                       <td>$singleRecord->gender</td>
                       <td>$singleRecord->bangla_mark</td>
                       <td>$singleRecord->english_mark</td>
                       <td>$singleRecord->math_mark</td>
                       <td>$singleRecord->remarks</td>
                       <td width='400px'>
               
                <a style='width: 60px' class='btn btn-success btn-sm' href='view.php?id=$singleRecord->id'>View</a>
                <a  style='width: 60px' class='btn btn-primary btn-sm' href='edit.php?id=$singleRecord->id'>Edit</a>
             
            
                      
             ";


            if($singleRecord->is_trashed=='NO')
            {

                echo "  <a  style='width: 60px' href='trash.php?id=$singleRecord->id'  class='btn btn-warning btn-sm'>Trash</a>";
            }
            else
            {
                echo "<a style='width: 60px'  class='btn btn-success btn-sm' href='recover.php?id=$singleRecord->id'>Recover</a>";
            }



            echo "                
                         <a style='width: 60px' class='btn btn-danger btn-sm' href='delete.php?id=$singleRecord->id' onclick=\"return confirm('Are you sure you want to delete record of id#$singleRecord->id?');\">Delete</a>                   
                        </td>
                   
                   </tr>
           
             ";
            $serial++;

        }


        ?>

        </tbody>
        <tfoot>
        <tr>
            <th></th>




            <th>  Serial </th>
            <th>  ID </th>
            <th>  First Name </th>
            <th>  Last Name </th>
            <th>Picture</th>
            <th>Home District</th>
            <th>Hobbies</th>
            <th>  Roll </th>
            <th>  Date of Birth </th>
            <th>  Gender </th>
            <th>Bangla</th>
            <th>English</th>
            <th>Math</th>
            <th>Remarks</th>
            <th>Action Buttons</th>



        </tr>
        </tfoot>

    </table>

</form>



<!-- first jQuery -->
<!-- then dataTables -->
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />



<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script>



    $('#DataTable').DataTable( {
        "pageLength": 5
    } );
</script>





<?php
ob_end_flush()
?>


<script>

    var selectedButtons;

    function enabledOrDisabledSelectedButtons() {


        window.selectedButtons = false;

        $('.checkboxs').each(function(){ //iterate all listed checkbox items
            if(this.checked) {
                window.selectedButtons=true;
            }
        });

        if(!window.selectedButtons){
            $("#TrashSelected").attr("disabled","disabled");
            $("#RecoverSelected").attr("disabled","disabled");
            $("#DeleteSelected").attr("disabled","disabled");
        }
        else{
            $("#TrashSelected").removeAttr('disabled');
            $("#RecoverSelected").removeAttr('disabled');
            $("#DeleteSelected").removeAttr('disabled');
        }


    }






    $(document).ready(function () {




        $('#TrashSelected').click(function () {
            document.forms[0].action  = "trash_selected.php";
            document.forms[0].submit();

        });

        $('#RecoverSelected').click(function () {
            document.forms[0].action  = "recover_selected.php";
            document.forms[0].submit();

        });

        $('#DeleteSelected').click(function () {
            document.forms[0].action  = "delete_selected.php";
            document.forms[0].submit();
        });



        enabledOrDisabledSelectedButtons();






////////////////////////////////////////////////////////////////////


        //select all checkboxes
        $("#SelectAll").change(function(){  //"select all" change
            var myStatus = this.checked; // "select all" checked status

            $('.checkboxs').each(function(){ //iterate all listed checkbox items
                this.checked = myStatus; //change ".MultipleCheckBox" checked status
            });

            enabledOrDisabledSelectedButtons();
        });

        $('.checkboxs').change(function(){ //".MultipleCheckBox" change


            enabledOrDisabledSelectedButtons();





//uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#SelectAll")[0].checked = false; //change "select all" checked status to false
            }

//check "select all" if all checkbox items are checked
            if ($('.checkboxs:checked').length == $('.checkboxs').length ){
                $("#SelectAll")[0].checked = true; //change "select all" checked status to true
            }
        });
    });
    /////////////////////////////////////////////////////////////////////////



    function myFunction(id) {
        var txt;
        var r = confirm("Are you sure your want to delete id#"+id+ " ?");

        return r;

    }



</script>