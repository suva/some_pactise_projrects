<?php
require_once "../vendor/autoload.php";

use \App\Utility;
use \App\Message;



$objStudent = new \App\Student();

$allOK = true;

foreach ($_POST['IDs'] as $eachID)
{
    $objStudent->setData(["id"=>$eachID]);
    $result = $objStudent->trash();

    if(!$result){
        $allOK = false;
    }
}


if($allOK){

    Message::message("All selected data has been trashed successfully");
}
else{
    Message::message("All selected data has not been trashed due to error(s)");

}
Utility::redirect("index.php");