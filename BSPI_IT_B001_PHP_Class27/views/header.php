<head>

    <script src="../resources/jquery-3.3.1.min.js"></script>
    <script src="../resources/jquery-ui.js"></script>
    <link rel="stylesheet" href="../resources/jquery-ui.css">

    <script src="../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <style>
        @import url('https://fonts.googleapis.com/css?family=Lobster');
    </style>

</head>





<div class="nav navbar-default">

  <div class="container">

      <a href="" class="navbar-brand">
          <span style="font-family: 'Lobster', cursive; font-size: 5vw">Code Station</span>
      </a>

      <div class="navbar-right">
          <a class="btn btn-primary" href="index.php">Home</a>
          <a class="btn btn-warning" href="trashlist.php">Trash List</a>
          <a class="btn btn-info"href="create.php">Add New Student</a>

      </div>



  </div>


</div>