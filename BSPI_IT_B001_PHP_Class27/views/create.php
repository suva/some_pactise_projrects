<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Form</title>

    <script src="../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

</head>
<body>
  <?php require_once "header.php"?>


  <div class="container">

      <h1>Student's Information Collection Form</h1>

      <form action="store.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
              <label for="FirstName">First Name</label>
              <input required type="text" class="form-control" name="FirstName" placeholder="Enter First Name Here...">
          </div>

          <div class="form-group">
              <label for="LastName">Last Name</label>
              <input required type="text" class="form-control" name="LastName" placeholder="Enter Last Name Here...">
          </div>


          <div class="form-group">
              <label for="ProfilePicture">Profile Picture</label>
              <input required type="file"  class="form-control" name="ProfilePicture" >
          </div>



          <div class="form-group">
              <label for="Hobbies">Hobbies: </label>
              <input  type="checkbox"  name="Hobbies[]" value="Reading" > Reading
              <input  type="checkbox"  name="Hobbies[]" value="Music" >   Music
              <input  type="checkbox"  name="Hobbies[]" value="Horse Riding" >  Horse Riding
              <input  type="checkbox"  name="Hobbies[]"   value="Programming" >  Programming
          </div>



          <div class="form-group">
              <label for="HomeDistrict">Home District: </label>
              <select required name="HomeDistrict" id="HomeDistrict">
                  <option value="">Select Home District</option>
                  <option value="Chittagong">Chittagong</option>
                  <option value="Dhaka">Dhaka</option>
                  <option value="Noakhali">Noakhali</option>
                  <option value="Barisal">Barisal</option>
                  <option value="Luxmipur">Luxmipur</option>
                  <option value="Joshore">Joshore</option>
                  <option value="Cox's Bazar">Cox's Bazar</option>
              </select>
          </div>



          <div style="position: relative; left:-13px" class="col-lg-6">
              <label for="RollNumber">Roll Number</label>
              <input required type="text" class="form-control" name="RollNumber" placeholder="Enter Roll Number Here...">
          </div>



          <div style="position: relative; left:13px" class="col-lg-6">
              <label for="DOB">Date of Birth</label>
              <input required type="date" class="form-control" name="DOB">
          </div>

          <p>
              &nbsp;
          </p>


          <div class="form-group">
              <label for="Gender">Gender</label>
              <input type="radio" id="Gender" name="Gender" value="Male"> Male
              <input required type="radio"  id="Gender" name="Gender" value="Female"> Female
          </div>




          <div style="position: relative; left:-13px" class="col-lg-4">
              <label for="BanglaMark">Bangla Mark</label>
              <input required type="text" class="form-control" name="BanglaMark" placeholder="Enter Bangla Mark Here...">
          </div>


          <div style="position: relative; left:13px" class="col-lg-4">
              <label for="EnglishMark">English Mark</label>
              <input required type="text" class="form-control" name="EnglishMark" placeholder="Enter English Mark Here...">
          </div>


          <div style="position: relative; left:13px" class="col-lg-4">
              <label for="MathMark">Math Mark</label>
              <input required type="text" class="form-control" name="MathMark" placeholder="Enter Math Mark Here...">
          </div>


          <div style="position: relative; left:-13px" class="col-lg-12">
              <p></p>
              <label for="Remarks">Remarks</label>
              <p></p>
              <textarea name="Remarks" id="Remarks" class="form-control"  rows="3"></textarea>
          </div>
          <br>

          <input type="submit">

      </form>



  </div>


</body>
</html>

