<?php

   require_once "../vendor/autoload.php";

   $objStudent = new \App\Student();


   $fnArr =  explode(".", $_FILES['ProfilePicture']['name']);

   $fileName =   $fnArr[0].time().".".$fnArr[1];

   $source = $_FILES['ProfilePicture']['tmp_name'];
   $destination = "../resources/ProfilePictures/" . $fileName;
   move_uploaded_file($source , $destination);


   $_POST['ProfilePicture'] = $fileName;

   $_POST['Hobbies'] = implode(", ", $_POST['Hobbies']);

   $objStudent->setData($_POST);

   $objStudent->store();

   \App\Utility::redirect("index.php");
