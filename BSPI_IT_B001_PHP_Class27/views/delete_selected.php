<?php
require_once "../vendor/autoload.php";

use \App\Utility;
use \App\Message;



$objStudent = new \App\Student();

$allOK = true;

foreach ($_POST['IDs'] as $eachID)
{
    $objStudent->setData(["id"=>$eachID]);
    $result = $objStudent->delete();

    if(!$result){
        $allOK = false;
    }
}


if($allOK){

    Message::message("All selected data has been deleted successfully");
}
else{
    Message::message("All selected data has not been deleted due to error(s)");

}
Utility::redirect("index.php");