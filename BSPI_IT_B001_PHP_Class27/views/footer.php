

<script>


    var selectedButtons;

    function enabledOrDisabledSelectedButtons() {


        window.selectedButtons = false;

        $('.checkbox').each(function(){ //iterate all listed checkbox items
            if(this.checked) {
                window.selectedButtons=true;
            }
        });

        if(!window.selectedButtons){
            $("#RecoverSelected").attr("disabled","disabled");
            $("#TrashSelected").attr("disabled","disabled");
            $("#DeleteSelected").attr("disabled","disabled");
        }
        else{
            $("#RecoverSelected").removeAttr('disabled');
            $("#TrashSelected").removeAttr('disabled');
            $("#DeleteSelected").removeAttr('disabled');
        }


    }




    $(document).ready(function () {



        ///////////////////////////////////////////////////////////////////////////
        $('#TrashSelected').click(function () {
            document.forms[1].action = "trash_selected.php";
            document.forms[1].submit();

        });

        $('#RecoverSelected').click(function () {
            document.forms[1].action = "recover_selected.php";
            document.forms[1].submit();

        });

        $('#DeleteSelected').click(function () {

            var result = confirm("Are you sure, you want to delete the selected records?")
            if(result){
                document.forms[1].action = "delete_selected.php";
                document.forms[1].submit();
            }

        });


        enabledOrDisabledSelectedButtons();

        ////////////////////////////////////////////////////////////////////////////






        /////////////////////////////////////////////////////////////////////////

        $('#SelectAll').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;

                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;

                });
            }

            enabledOrDisabledSelectedButtons();

        });

        $('.checkbox').on('click', function () {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#SelectAll').prop('checked', true);

            } else {
                $('#SelectAll').prop('checked', false);

            }

            enabledOrDisabledSelectedButtons();
        });




        ////////////////////////////////////////////////////////////////////






        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);



    });


    function confirmDelete(id) {
        return  confirm("Are you sure you want to delete record id#"+id);
    }




</script>




<!-- required for search, block 3 of 5 start -->
<script>

    $(function() {
        var availableTags = [
            <?php
            echo $comma_separated_keywords;
            ?>
        ];

        // Filter function to search only from the beginning of the string

        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });

        $("#searchID").autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block3 of 5 end -->

