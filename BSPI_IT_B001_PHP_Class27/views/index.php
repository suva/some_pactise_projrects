<?php
  require_once "../vendor/autoload.php";

  $objStudent = new \App\Student();
  $allActiveStudentsRecords =  $objStudent->index();

################## search  block 1 of 5 start ##################

$availableKeywords = $objStudent->getAllKeywords();


$comma_separated_keywords = '"'.implode('","',$availableKeywords).'"';


################## search  block 1 of 5 end ##################



?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Form</title>

</head>
<body>

<?php require_once "header.php"?>

<form action=""></form>


<div class="container">



    <div class="col-md-8">
        <button id="TrashSelected" class="btn btn-warning">Trash Selected</button>
        <button id="DeleteSelected" class="btn btn-danger">Delete Selected</button>


        <a class="btn btn-info"href="pdf.php">Download As PDF</a>
        <a class="btn btn-info"href="xl.php">Download As XL</a>
        <a class="btn btn-info"href="email.php">Email This List</a>
    </div>


    <!-- required for search, block 2 of 5 start -->
    <div class="col-md-4"   >

        <form id="searchForm" action="searched.php"  method="get" style="margin-top: 5px; margin-bottom: 10px ">
            <input class="form-control" " type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
            <input type="checkbox"  name="byName"   checked  >By Name
            <input type="checkbox"  name="byRoll"  checked >By Roll
            <input type="checkbox"  name="byGender"  checked >By Gender
            <input hidden type="submit" class="btn-primary" value="search">
        </form>
    </div>
    <!-- required for search, block 2 of 5 end --></div>




<div style="height: 30px">
        <div id="MsgDiv" class="well-lg" style="text-align: center"><?= \App\Message::getMessage()  ?></div>
    </div>

    <h1>Student's Information - Active List </h1>


<!--////////////////////////////////Pagination Start //////////////////////////////////////////////-->

<?php

if (isset($_GET['pageNo'])) {
    $pageNo = $_GET['pageNo'];
} else {
    $pageNo = 1;
}
$itemPerPage =2;

$offset = ($pageNo-1) * $itemPerPage;

$totalPages = ceil(count($allActiveStudentsRecords) / $itemPerPage);

$someRecords = $objStudent->paginateActiveList($offset,$itemPerPage);

?>

<ul class="Middle pagination">
    <li><a href="?pageNo=1">First</a></li>
    <li class="<?php if($pageNo <= 1){ echo 'disabled'; } ?>">
        <a href="<?php if($pageNo <= 1){ echo '#'; } else { echo "?pageNo=".($pageNo - 1); } ?>">Prev</a>
    </li>
    <li class="<?php if($pageNo >= $totalPages){ echo 'disabled'; } ?>">
        <a href="<?php if($pageNo >= $totalPages){ echo '#'; } else { echo "?pageNo=".($pageNo + 1); } ?>">Next</a>
    </li>
    <li><a href="?pageNo=<?php echo $totalPages; ?>">Last</a></li>
</ul>

<!--////////////////////////////////Pagination End //////////////////////////////////////////////-->





    <table class="table table-bordered table-striped">

        <tr>

            <th width='20px'> <input id = "SelectAll" type="checkbox"> Check All</th>
            <th>Serial</th>
            <th>ID</th>
            <th>Full Name</th>
            <th>Roll</th>
            <th>DOB</th>
            <th>Gender</th>
            <th>Bangla</th>
            <th>English</th>
            <th>Math</th>
            <th width='300px'>Action Buttons</th>
        </tr>

        <form action="" method="post">
        <?php

             $serial =($pageNo-1) * $itemPerPage +1 ;
             
             foreach($someRecords as $eachStudentRecord){


                 echo "
                
                     <tr>
                     
                          <td width='20px'><input class='checkbox' name='IDs[]' value='$eachStudentRecord->id' type='checkbox'> </td>
                          <td>$serial</td>
                          <td>$eachStudentRecord->id</td>
                          <td>$eachStudentRecord->first_name $eachStudentRecord->last_name</td>
                          <td>$eachStudentRecord->roll </td>
                          <td>$eachStudentRecord->dob </td>
                          <td>$eachStudentRecord->gender </td>
                          <td>$eachStudentRecord->bangla_mark </td>
                          <td>$eachStudentRecord->english_mark </td>
                          <td>$eachStudentRecord->math_mark </td>
                     
                     
                          <td width='300px'>
                          
                              <a class='btn btn-info' href='view.php?id=$eachStudentRecord->id'>View</a>
                              <a class='btn btn-primary' href='edit.php?id=$eachStudentRecord->id'>Edit</a>
                              <a class='btn btn-warning' href='trash.php?id=$eachStudentRecord->id'>Trash</a>
                              <a class='btn btn-danger' onclick='return confirmDelete($eachStudentRecord->id)' href='delete.php?id=$eachStudentRecord->id'>Delete</a>
                          
                          
                          </td>
                     
                 
                     </tr>
                 ";

                 $serial++;
             }

        ?>
        </form>    

    </table>

</div>

<?php
  require_once "footer.php";
?>


</body>
</html>

