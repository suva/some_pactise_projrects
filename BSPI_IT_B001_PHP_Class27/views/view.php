<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>
<?php require_once "header.php"?>
<div class="container">



    <?php
  //  require_once "header.php";
    require_once "../vendor/autoload.php";

    $objStudent = new \App\Student();


    $objStudent->setData($_GET);
    $singleRecord = $objStudent->view();


    $fullName = $singleRecord->first_name . " " . $singleRecord->last_name;

    $id = $singleRecord->id;
    $dob = $singleRecord->dob;
    $gender = $singleRecord->gender;
    $roll = $singleRecord->roll;
    $banglaMark = $singleRecord->bangla_mark;
    $englishMark = $singleRecord->english_mark;
    $mathMark = $singleRecord->math_mark;
    $homeDistrict = $singleRecord->home_district;
    $hobbies = $singleRecord->hobbies;
    $remark = $singleRecord->remarks;
    $profilePicture = $singleRecord->profile_picture;
    $template = <<<SingleRecordTemplate

<table class="table table-bordered table-stripped">

     <tr>
         <th style="padding: 30px;" colspan="2"> <h1>Single Record of : $fullName </h1> </th>
         <th style="text-align: center" colspan="3"> <img width='130' height='140' src="../resources/ProfilePictures/$profilePicture"/> </th>
     </tr>
    
    <tr >
        <td width="50%"><strong>ID: </strong>$id</td>  
        <td colspan="2"><strong>DOB: </strong>$dob</td>
        
    </tr>   
    
    <tr >
        <td width="50%"><strong>Gender: </strong>$gender</td>  
        <td colspan="2"><strong>Roll: </strong>$roll</td>
    </tr>
    
    <tr >
        <td width="50%"><strong>Home District: </strong>$homeDistrict</td>  
        <td colspan="2"><strong>Hobies: </strong>$hobbies</td>
    </tr>
    
    <tr>
        <td class="col-md-4"><strong>Bangla Mark: </strong>$banglaMark</td>
        <td class="col-md-4"><strong>English Mark: </strong>$englishMark</td>
        <td class="col-md-4"><strong>Math Mark: </strong>$mathMark</td>
    </tr>
    
     <tr>
         <th style="text-align: left" colspan="3"> <h4><strong>Remark : </strong>$remark </h4> </th>
     </tr>
</table>


SingleRecordTemplate;


    echo $template;

    ?>


</div>

</body>
</html>