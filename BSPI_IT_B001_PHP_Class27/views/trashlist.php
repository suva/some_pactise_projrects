<?php
  require_once "../vendor/autoload.php";

  $objStudent = new \App\Student();
  $allTrashedStudentsRecords =  $objStudent->getTrashList();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Form</title>

</head>
<body>

<?php require_once "header.php"?>

<form action=""></form>

<div class="container">

    <div class="col-md-8">
        <button id="RecoverSelected" class="btn btn-success">Recover Selected</button>
        <button id="DeleteSelected" class="btn btn-danger">Delete Selected</button>


        <a class="btn btn-info"href="pdf.php">Download As PDF</a>
        <a class="btn btn-info"href="xl.php">Download As XL</a>
        <a class="btn btn-info"href="email.php">Email This List</a>
    </div>





    <div style="height: 30px">
        <div id="MsgDiv" class="well-lg" style="text-align: center"><?= \App\Message::getMessage()  ?></div>
    </div>

    <h1>Student's Information - Trash List </h1>


    <table class="table table-bordered table-striped">

        <tr>
            <th width='20px'> <input id = "SelectAll" type="checkbox"> Check All</th>
            <th>Serial</th>
            <th>ID</th>
            <th>Full Name</th>
            <th>Roll</th>
            <th>DOB</th>
            <th>Gender</th>
            <th>Bangla</th>
            <th>English</th>
            <th>Math</th>
            <th>Action Buttons</th>
        </tr>

        <form action="" method="post">
        <?php
             $serial = 0;
             foreach($allTrashedStudentsRecords as $eachStudentRecord){
                 $serial++;
                 echo "
                 
                     <tr>
                     
                           <td width='20px'><input class='checkbox' name='IDs[]' value='$eachStudentRecord->id' type='checkbox'> </td>
                          <td >$serial</td>
                          <td>$eachStudentRecord->id</td>
                          <td>$eachStudentRecord->first_name $eachStudentRecord->last_name</td>
                          <td>$eachStudentRecord->roll </td>
                          <td>$eachStudentRecord->dob </td>
                          <td>$eachStudentRecord->gender </td>
                          <td>$eachStudentRecord->bangla_mark </td>
                          <td>$eachStudentRecord->english_mark </td>
                          <td>$eachStudentRecord->math_mark </td>
                     
                     
                          <td>
                          
                              <a class='btn btn-info' href='view.php?id=$eachStudentRecord->id'>View</a>
                              <a class='btn btn-primary' href='edit.php?id=$eachStudentRecord->id'>Edit</a>
                              <a class='btn btn-success' href='recover.php?id=$eachStudentRecord->id'>Recover</a>
                              <a class='btn btn-danger' onclick='return confirmDelete($eachStudentRecord->id)' href='delete.php?id=$eachStudentRecord->id'>Delete</a>
                          
                          
                          </td>
                     
                 
                     </tr>
                 ";
             }

        ?>

        </form>
    </table>

</div>

<script src="../resources/jquery-3.3.1.min.js"></script>

<script>

    $(document).ready(function () {


        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);
        $("#MsgDiv").fadeIn(200);
        $("#MsgDiv").fadeOut(500);



    });


    function confirmDelete(id) {
        return  confirm("Are you sure you want to delete record id#"+id);
    }


</script>

<?php
    require_once "footer.php";
?>

</body>
</html>

