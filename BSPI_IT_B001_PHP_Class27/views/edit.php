


<?php
  //  require_once "header.php";
    require_once "../vendor/autoload.php";

    $objStudent = new \App\Student();


    $objStudent->setData($_GET);
    $singleRecord = $objStudent->view();




?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Form</title>

    <script src="../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

</head>
<body>
  <?php require_once "header.php"?>


  <div class="container">

      <h1>Student's Information Collection Form</h1>

      <form action="update.php" method="post" enctype="multipart/form-data">

          <input type="hidden" name="id" value="<?= $_GET['id']?>">

         <div class="form-group">
              <label for="FirstName">First Name</label>
              <input required type="text" class="form-control" name="FirstName" value="<?=$singleRecord->first_name?>">
          </div>

          <div class="form-group">
              <label for="LastName">Last Name</label>
              <input required type="text" class="form-control" name="LastName" value="<?=$singleRecord->last_name?>">
          </div>


          <div class="form-group">
              <div>
                  <img src="../resources/ProfilePictures/<?=$singleRecord->profile_picture?>" width="100px" height="100px" alt="">
              </div>
              <label for="ProfilePicture">Profile Picture</label>
              <input  type="file"  class="form-control" name="ProfilePicture" >
          </div>



          <div class="form-group">
              <label for="Hobbies">Hobbies: </label>
              <input  type="checkbox"  name="Hobbies[]" value="Reading" <?php if(strpos($singleRecord->hobbies,"Reading")!==false) echo "checked"?> > Reading
              <input  type="checkbox"  name="Hobbies[]" value="Music" <?php if(strpos($singleRecord->hobbies,"Music")!==false) echo "checked"?> >    Music
              <input  type="checkbox"  name="Hobbies[]" value="Horse Riding" <?php if(strpos($singleRecord->hobbies,"Horse Riding")!==false) echo "checked"?> >   Horse Riding
              <input  type="checkbox"  name="Hobbies[]"   value="Programming" <?php if(strpos($singleRecord->hobbies,"Programming")!==false) echo "checked"?> >  Programming
          </div>



          <div class="form-group">
              <label for="HomeDistrict">Home District: </label>
              <select required name="HomeDistrict" id="HomeDistrict">
                  <option value="">Select Home District</option>
                  <option value="Chittagong" <?php if($singleRecord->home_district=="Chittagong") echo "selected"?>>Chittagong</option>
                  <option value="Dhaka" <?php if($singleRecord->home_district=="Dhaka") echo "selected"?>>Dhaka</option>
                  <option value="Noakhali" <?php if($singleRecord->home_district=="Noakhali") echo "selected"?>>Noakhali</option>
                  <option value="Barisal" <?php if($singleRecord->home_district=="Barisal") echo "selected"?>>Barisal</option>
                  <option value="Luxmipur" <?php if($singleRecord->home_district=="Luxmipur") echo "selected"?>>Luxmipur</option>
                  <option value="Joshore" <?php if($singleRecord->home_district=="Joshore") echo "selected"?>>Joshore</option>
                  <option value="Cox's Bazar" <?php if($singleRecord->home_district=="Cox's Bazar") echo "selected"?>>Cox's Bazar</option>
              </select>
          </div>



          <div style="position: relative; left:-13px" class="col-lg-6">
              <label for="RollNumber">Roll Number</label>
              <input required type="text" class="form-control" name="RollNumber" value="<?=$singleRecord->roll?>">
          </div>



          <div style="position: relative; left:13px" class="col-lg-6">
              <label for="DOB">Date of Birth</label>
              <input required type="date" class="form-control" name="DOB" value="<?=$singleRecord->dob?>">
          </div>

          <p>
              &nbsp;
          </p>



          <div class="form-group">
              <label for="Gender">Gender</label>
              <input type="radio" id="Gender" name="Gender" value="Male" <?php if($singleRecord->gender == "Male") echo "checked" ?> > Male
              <input required type="radio"  id="Gender" name="Gender" value="Female" <?php if($singleRecord->gender == "Female") echo "checked" ?>> Female
          </div>




          <div style="position: relative; left:-13px" class="col-lg-4">
              <label for="BanglaMark">Bangla Mark</label>
              <input required type="text" class="form-control" name="BanglaMark" value="<?=$singleRecord->bangla_mark?>">
          </div>


          <div style="position: relative; left:13px" class="col-lg-4">
              <label for="EnglishMark">English Mark</label>
              <input required type="text" class="form-control" name="EnglishMark" value="<?=$singleRecord->english_mark?>">
          </div>


          <div style="position: relative; left:13px" class="col-lg-4">
              <label for="MathMark">Math Mark</label>
              <input required type="text" class="form-control" name="MathMark" value="<?=$singleRecord->math_mark ?>">
          </div>


          <div style="position: relative; left:-13px" class="col-lg-12">
              <p></p>
              <label for="Remarks">Remarks</label>
              <p></p>
              <textarea name="Remarks" id="Remarks" class="form-control"  rows="3"><?php echo $singleRecord->remarks ?></textarea>
          </div>
          <br>

          <input type="submit" value = "Update">

      </form>



  </div>


</body>
</html>

