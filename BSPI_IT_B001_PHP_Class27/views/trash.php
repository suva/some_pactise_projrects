<?php

require_once "../vendor/autoload.php";
use App\Utility;

$objStudent = new \App\Student();

$objStudent->setData($_GET);

$result = $objStudent->trash();


if($result){

    Message::message("Data has been trashed successfully");
}
else{
    Message::message("Data has not been trashed due to error(s)");

}

Utility::redirect("index.php");


