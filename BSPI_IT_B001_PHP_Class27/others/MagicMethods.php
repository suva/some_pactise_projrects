<?php


  class MyClass{

      public function __invoke()
      {
         echo  "You are trying to call an Object, Are you crazy? <hr>";
      }

      public function __toString()
      {
         return "You are trying to echo an Object, Are you crazy? <hr>";
      }

      public function __clone()
      {
          echo "I'm inside the ". __METHOD__."<br>";
      }

      public function __get($name)
      {
          echo "I'm inside the ". __METHOD__."<br>";
          echo "The wrong property name $name <hr>";
      }

      public function __set($name, $value)
      {
          echo "I'm inside the ". __METHOD__."<br>";
          echo "The wrong property name $name <br>";
          echo "The wrong property value $value <hr>";
      }

      public function __unset($name)
      {
          echo "I'm inside the ". __METHOD__."<br>";
          echo "The wrong property name $name <hr>";
      }

      public function __isset($name)
      {
          echo "I'm inside the ". __METHOD__."<br>";
          echo "The wrong property name $name <hr>";
      }


      public function __construct()
      {
          echo "I'm inside the ". __METHOD__."<hr>";
      }


      public function __destruct()
      {
          echo "I'm inside the ". __METHOD__."<hr>";

      }


      public function __call($name, $arguments)
      {
         echo "The wrong method name : $name<br>";
         echo "The arguments are: <br>";
         print_r($arguments);
         echo "<hr>";

      }



      public static function __callStatic($name, $arguments)
      {
         echo "The wrong static method name : $name<br>";
         echo "The arguments are: <br>";
         print_r($arguments);
         echo "<hr>";

      }


      public static function doSomethingStatic(){

          echo "I'm inside the ". __METHOD__."<hr>";
      }

      public $myValue;

      public function doSomething()
      {

          echo "The value is $this->myValue <br>";
      }

  }




  MyClass::doSomethingStatic();
  MyClass::doSomethingStatic123(23,42,2,42);


  $obj = new MyClass();

   echo $obj;
   $obj();

  echo $obj->varXYZ;


   $obj->varXYZ  = 343;

  $obj->myValue = 555;
$obj1 =  clone $obj;

  echo $obj1->myValue ."sldfjslfdkjslfdj skfdjslkfdjlsjflskdjf";

  unset($obj->sdlkfjsljlkjdrklsjer);

  if(isset($obj->aPropertyThatDoesNotExist)){


  }



  $obj->aMethodThatDoesnotExit1(23,"Hello",true);
  $obj->aMethodThatDoesnotExit2();
  unset($obj);
  echo "Hello World <hr>";