<?php
 namespace App;

 echo __FILE__ ."<br>";
 echo __DIR__ ."<br>";
 echo __LINE__ ."<br>";


 class MyClass{
     use MyTrait;
     public function doSomething(){
         echo __CLASS__ ."<br>";
         echo __METHOD__ ."<br>";
     }


 }


 trait MyTrait{

      public $myTp1;

      public function doSthInTr(){
          echo __TRAIT__ ."<br>";
      }

 }


 $obj = new MyClass();
 $obj->doSomething();

 $obj->doSthInTr();


 echo __NAMESPACE__ ."<br>";
