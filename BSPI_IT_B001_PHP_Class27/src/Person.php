<?php
namespace App;

class Person extends Database {

    protected $firstName ;
    protected $lastName;
    protected $profilePicture;
    protected $hobbies;
    protected $homeDistrict;
    protected $dob;
    protected $gender;





    public function setData($dataArray){


        if(array_key_exists("id",$dataArray)){
            $this->id = $dataArray['id'];
        }

        if(array_key_exists("FirstName",$dataArray)){
            $this->firstName = $dataArray['FirstName'];
        }


        if(array_key_exists("LastName",$dataArray)){
            $this->lastName = $dataArray['LastName'];
        }

        if(array_key_exists("ProfilePicture",$dataArray)){
            $this->profilePicture = $dataArray['ProfilePicture'];
        }

        if(array_key_exists("Hobbies",$dataArray)){
            $this->hobbies = $dataArray['Hobbies'];
        }

        if(array_key_exists("HomeDistrict",$dataArray)){
            $this->homeDistrict = $dataArray['HomeDistrict'];
        }

        if(array_key_exists("DOB",$dataArray)){
            $this->dob = $dataArray['DOB'];
        }

        if(array_key_exists("Gender",$dataArray)){
            $this->gender = $dataArray['Gender'];
        }



    }


}// end of Person Class
