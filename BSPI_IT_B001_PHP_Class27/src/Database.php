<?php
/**
 * Created by PhpStorm.
 * User: Tushar Chowdhury
 * Date: 5/26/2019
 * Time: 12:17 PM
 */

namespace App;
use PDO;
use PDOException;

class Database
{

    public $dbh;

    public function __construct()
    {


        try {
            $this->dbh = new PDO('mysql:host=localhost;dbname=atomic_project_bspi_it_b001', "root", "");

           // echo "Database connection successfull<br>";

        } catch (PDOException $e) {
            print "Error!Code Station: " . $e->getMessage() . "<br/>";
            die();
        }

    }


}