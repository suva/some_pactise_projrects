<?php
/**
 * Created by PhpStorm.
 * User: Tushar Chowdhury
 * Date: 7/6/2019
 * Time: 2:48 PM
 */

namespace App;


class Utility
{

    public static function d($myVar){
        echo "<pre>";
        var_dump($myVar);
        echo "</pre>";
    }

    public static function dd($myVar){
        echo "<pre>";
        var_dump($myVar);
        echo "</pre>";
        die;
    }

    public static function redirect($url){
        $path = "Location: $url";
        header($path);
    }

}