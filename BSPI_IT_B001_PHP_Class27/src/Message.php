<?php
/**
 * Created by PhpStorm.
 * User: Tushar Chowdhury
 * Date: 5/30/2019
 * Time: 9:45 AM
 */

namespace App;

if(!isset($_SESSION)) session_start();

class Message
{
    public static function  message($msg = NULL){

        if($msg==NULL){

           return  self::getMessage();
        }
        else{

            self::setMessage($msg);
        }
    }

    public static function setMessage($msg){
      $_SESSION['message'] = $msg;
    }

    public static function getMessage(){

        if(isset($_SESSION['message'])){

            $tempMessage = $_SESSION['message'];
            $_SESSION['message']= "";
            return $tempMessage;

        }
        else
            return "";

    }



}