<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="resource/bootstrap/css/bootstrap.min.css">

    <script src="resource/js/bootstrap.min.js"></script>
    <title>admin</title>
</head>
<body>
<div style="margin: auto;
  width: 50%;
  border: 3px solid green;
  padding: 10px;">
<form method="post" action="authn.php"  >
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email"  name="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</body>
</html>