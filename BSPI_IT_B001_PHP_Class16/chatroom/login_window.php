<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Window</title>

    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="style.css">

</head>
<body>

<div class="container">


    <div id="LoginWindow">

        <div  id="LoginBox">

            <form action="" method="post">
                <div class="form-group">
                    <label for="ChatterName">Chatter Name</label>
                    <input type="text" class="form-control" id="ChatterName" name="ChatterName"placeholder="Enter Your Chat Name">
                    <small id="chatterHelp" class="form-text text-muted">Please do not abuse our chatroom</small>
                </div>

                <button type="submit" class="btn btn-primary">Enter</button>
            </form>

        </div>

    </div>



</div>



<script src="bootstrap-4.3.1-dist/jquery-3.3.1.min.js" s></script>
<script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
</body>
</html>