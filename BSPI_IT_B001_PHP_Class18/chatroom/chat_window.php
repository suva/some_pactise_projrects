<?php
if(!isset($_SESSION))  session_start();
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat Window</title>

    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">

<div id="ChatWindow">

    <div id="Header">

        <h2 style="display: inline-block">Welcome to Code Station Chat Room - BSPI-IT-B001 </h2>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <h3 style="color:#ff0119; display: inline-block">Hi <?= $_SESSION['ChatterName']."!"       ?>   </h3>  <a  href='logoff.php'><h2 class="btn btn-danger" style="display: inline-block">LogOff</h2></a>

    </div>

    <div id="Middle">
        <div id="ChatHistory">

            <strong>Chat History</strong>
            <br>

            <textarea disabled name="ChatHistoryText" id="ChatHistoryText" cols="80" rows="10"></textarea>


        </div>


        <div id="ChatterList">

            <strong>Chatter List</strong>
            <br>

            <textarea disabled name="ChatterListText" id="ChatterListText" cols="30" rows="10"></textarea>

        </div>


    </div>


    <div id="Footer" >
       <div style="display: inline-block">
           <input type="text" id="msg" class="form-control" size="100">
       </div>

        <div style="display: inline-block">
            <input type="submit" class="btn btn-success" style="display: inline-block"  value="Send Message">
       </div>



    </div>


</div>



</div>




<script src="bootstrap-4.3.1-dist/jquery-3.3.1.min.js" s></script>
<script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>


<script>


    setInterval(function () {

     $.ajax({url: "chathistory.txt", success: function(result){

            $("#ChatHistoryText").html(result);
        }});


         percent = Math.floor( ((   ($("#ChatHistoryText").scrollTop() + document.getElementById("ChatHistoryText").offsetHeight) / $("#ChatHistoryText").prop("scrollHeight") ) * 100 ));


        if(percent>90){
            scrollDown();
        }


    },500) ;


    $("#msg").keypress(function(event){


       if(event.keyCode==13) {

           $.post(
               "write2file.php",
               {
                   ChatterName: "<?= $_SESSION['ChatterName'] ?>",
                   MSG: document.getElementById("msg").value
               },

               function(data) {
                   //alert(data)
               }
           );
       }

    });


    function scrollDown() {
        //        document.getElementById("ChatHistoryText").scrollTop =   document.getElementById("ChatHistoryText").scrollHeight;

        $("#ChatHistoryText").scrollTop(   $("#ChatHistoryText").prop("scrollHeight")   );
    }



</script>



</body>
</html>