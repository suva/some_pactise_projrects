
<?php require_once "authenticate.php"; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Information</title>

    <link rel="stylesheet" href="resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">

    <h1>Student Information Collection Form </h1>

    <form action="process.php" method="post" enctype="multipart/form-data">


        <div class="form-group">
            <label for="FirstName">First Name</label>
            <input type="text" class="form-control" name="FirstName" placeholder="Enter your first name here...">
        </div>

        <div class="form-group">
            <label for="LastName">Last Name</label>
            <input type="text" class="form-control" name="LastName" placeholder="Enter your last name here...">
        </div>

        <div class="form-group">
            <label for="ProfilePicture">Profile Picture</label>
            <input type="file" name="ProfilePicture">
        </div>



        <div class="checkbox">

            <strong >Hobbies:</strong>

            <label>
                <input type="checkbox" name="Hobbies[]" value="Painting"> Painting
            </label>

            <label>
                <input type="checkbox" name="Hobbies[]" value="Horse Riding"> Horse Riding
            </label>


            <label>
                <input type="checkbox" name="Hobbies[]" value="Computer Gaming"> Computer Gaming
            </label>

        </div>



        <div class="form-group">

            <label for="HomeDistrict">Home District</label>

            <select class="form-control" name="HomeDistrict" id="HomeDistrict">

                <option value="">Please Select Your Home District</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Chattagram">Chattagram</option>
                <option value="Noakhali">Noakhali</option>
                <option value="Sylhet">Sylhet</option>
                <option value="Khulna">Khulna</option>
                <option value="Barisal">Barisal</option>

            </select>


        </div>


        <div class="form-group" style="margin-left: -15px">

            <div class="form-group col-md-6" >

                <label for="Roll">Roll</label>
                <input type="text" class="form-control" name="Roll" placeholder="Enter your roll here...">
            </div>



            <div class="form-group col-md-6" >
                <label for="DOB">DOB</label>
                <input type="date" class="form-control" name="DOB" >
            </div>


        </div>

        <div class="form-group">

            <strong>Gender: </strong>

            <label class="radio-inline">
                <input type="radio" name="Gender" id="Gender" value="Male">Male
            </label>

            <label class="radio-inline">
                <input type="radio" name="Gender" id="Gender" value="Female">Female
            </label>

        </div>



        <div class="form-group" style="margin-left: -15px">

            <div class="form-group col-md-4" >
                <label for="BanglaMark">Bangla Mark</label>
                <input type="number" min="0" max="100" class="form-control" name="BanglaMark" >
            </div>

            <div class="form-group col-md-4" >
                        <label for="EnglishMark">English Mark</label>
                        <input type="number" min="0" max="100" class="form-control" name="EnglishMark" >
                    </div>

            <div class="form-group col-md-4" >
                        <label for="MathMark">Math Mark</label>
                        <input type="number" min="0" max="100" class="form-control" name="MathMark" >
            </div>

        </div>



        <div class="form-group">
            <label for="Remarks">Remarks</label>
            <br>
            <textarea name="Remarks" id="Remarks" cols="130" rows="5">

            </textarea>

        </div>




        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>


</body>
</html>



