

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Information</title>

    <link rel="stylesheet" href="resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

</head>
<body>



<?php

  function mark2Grade($mark){

      if($mark>=80) return "A+";
      elseif($mark>=70) return "A";
      elseif($mark>=60) return "B";
      elseif($mark>=50) return "C";
      elseif($mark>=40) return "D";
      else return "F";

  }


$banglaGrade = mark2Grade($_POST['BanglaMark']);
$englishGrade = mark2Grade($_POST['EnglishMark']);
$mathGrade = mark2Grade($_POST['MathMark']);



$_POST['Hobbies'] = implode(", ", $_POST['Hobbies']);



  $template = file_get_contents("template.html");


  $template = str_replace('$#$FullName$#$',$_POST['FirstName']." ".$_POST['LastName'],$template);
  $template = str_replace('$#$Hobbies$#$',$_POST['Hobbies'],$template);
  $template = str_replace('$#$DOB$#$',$_POST['DOB'],$template);
  $template = str_replace('$#$HomeDistrict$#$',$_POST['HomeDistrict'],$template);
  $template = str_replace('$#$Roll$#$',$_POST['Roll'],$template);
  $template = str_replace('$#$Gender$#$',$_POST['Gender'],$template);
  $template = str_replace('$#$BanglaMark$#$',$_POST['BanglaMark'],$template);
  $template = str_replace('$#$BanglaGrade$#$',$banglaGrade,$template);
  $template = str_replace('$#$EnglishMark$#$',$_POST['EnglishMark'],$template);
  $template = str_replace('$#$EnglishGrade$#$',$englishGrade,$template);
  $template = str_replace('$#$MathMark$#$',$_POST['MathMark'],$template);
  $template = str_replace('$#$MathGrade$#$',$mathGrade,$template);
  $template = str_replace('$#$Remark$#$',$_POST['Remarks'],$template);


  $nameExtArr= explode(".",$_FILES['ProfilePicture']['name']);

  $fileName = $nameExtArr[0]. time(). "." . $nameExtArr[1];

  $source = $_FILES['ProfilePicture']['tmp_name']; //form thke img ta neya hcce
  $destination = "ProfilePictures/$fileName"; //img folder e jacche pic ta
  move_uploaded_file($source,$destination);



  $template = str_replace('$#$ProfilePicture$#$',$fileName,$template);


  echo "$template";

?>


</body>

</html>


